package com.edso.exercise50;

import java.util.*;

public class Main {
    private static Map<Integer, Location> locations = new HashMap<Integer, Location>();
    public static final int COMPUTER_LOCATION_ID = 0;
    public static final int ROAD_LOCATION_ID = 1;
    public static final int HILL_LOCATION_ID = 2;
    public static final int BUILDING_LOCATION_ID = 3;
    public static final int VALLEY_LOCATION_ID = 4;
    public static final int FOREST_LOCATION_ID = 5;

    //ham contructor khoi tao gia tri ban dau
    public Main() {

        //vi tri index trong Map locations trung voi id location cua vi tri trong class Location
        locations.put(0, new Location(COMPUTER_LOCATION_ID, "You are sitting on a desk"));
        locations.put(1, new Location(ROAD_LOCATION_ID, "You are standing at the end of a road"));
        locations.put(2, new Location( HILL_LOCATION_ID , "You are at the top of a hill"));
        locations.put(3, new Location(BUILDING_LOCATION_ID, "You are inside a building"));
        locations.put(4, new Location(VALLEY_LOCATION_ID, "You are in a valley"));
        locations.put(5, new Location(FOREST_LOCATION_ID, "You are in the forest"));

        locations.get(1).addExit("W", HILL_LOCATION_ID);
        locations.get(1).addExit("E", BUILDING_LOCATION_ID);
        locations.get(1).addExit("S", VALLEY_LOCATION_ID);
        locations.get(1).addExit("N", FOREST_LOCATION_ID);

        locations.get(2).addExit("N", FOREST_LOCATION_ID);

        locations.get(3).addExit("W", ROAD_LOCATION_ID);

        locations.get(4).addExit("N", ROAD_LOCATION_ID);
        locations.get(4).addExit("W", HILL_LOCATION_ID);

        locations.get(5).addExit("S", ROAD_LOCATION_ID);
        locations.get(5).addExit("W", HILL_LOCATION_ID);

    }

    //ham command cho phep nhap lenh day du
    public void command() {
        Scanner scanner = new Scanner(System.in);

        Map<String, String> vocabulary = new HashMap<String, String>();
        vocabulary.put("QUIT", "Q");
        vocabulary.put("NORTH", "N");
        vocabulary.put("SOUTH", "S");
        vocabulary.put("WEST", "W");
        vocabulary.put("EAST", "E");

        //vi tri ban dau la vi tri thu 1
        int location = ROAD_LOCATION_ID;
        while (true) {
            System.out.println(locations.get(location).getDescription());
            if (location == 0) {
                break;
            }

            //in ra cac vi tri co the di chuyen
            Map<String, Integer> exits = locations.get(location).getExits();
            System.out.print("Available exits are ");
            for (String exit : exits.keySet()) {
                System.out.print(exit + ", ");
            }
            System.out.println();

            //nhan gia tri nguoi dung nhap
            String direction = scanner.nextLine().toUpperCase();
            if (direction.length() > 1) {
                String[] words = direction.split(" ");
                for (String word : words) {
                    if (vocabulary.containsKey(word)) {
                        direction = vocabulary.get(word);
                        break;
                    }
                }
            }


            if (exits.containsKey(direction)) {
                location = exits.get(direction);

            } else {
                System.out.println("You cannot go in that direction");
            }
        }
    }

    public static void main(String[] args) {
        Main main = new Main();
        main.command();
    }
}
