package com.edso.exercise51;

import java.util.HashMap;
import java.util.Map;


public class Location {
    //cac truong deu khai bao la final va private
    private final int locationID;
    private final String description;
    private final Map<String, Integer> map;

    //handle truong hop map bi null
    public Location(int locationID, String description, Map<String, Integer> map) {
        this.locationID = locationID;
        this.description = description;
        if(map != null) {
            this.map = new HashMap<String, Integer>(map);
        } else {
            this.map = new HashMap<String, Integer>();
        }
        this.map.put("Q", 0);
    }

    //ko co ham set de thay doi du lieu
    public int getLocationID() {
        return locationID;
    }

    public String getDescription() {
        return description;
    }

    public Map<String, Integer> getExits() {
        return new HashMap<String, Integer>(map);
    }
}
